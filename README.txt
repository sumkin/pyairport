Description 
###########

Pyhon package helping to retrieve airport info based on codes.

Usage
#####

from pyairport.airport import Airport

Get all countries
-----------------
countries = Airport.get_countries()

Get all cities in country
-------------------------
cities = Airport.get_cities_in_country(country_code)

Create instance and find out in which city, country
---------------------------------------------------
airport = Airport(airport_code)
city = airport.get_city()
country = airport.get_country()

Find distnace between airports
------------------------------
ap1 = Airport(ap_code1)
ap2 = Airport(ap_code2)
d = ap1.distance(ap2)


