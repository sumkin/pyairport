import csv
import sqlite3

def sqlite_db():
  conn = sqlite3.connect('airports.db')
  curs = conn.cursor()
  return conn,curs

def create_table():
  conn,curs = sqlite_db()
  q = "CREATE TABLE data (\
         iata_code CHAR(3),\
         icao_code CHAR(4),\
         name_eng TEXT,\
         city_eng TEXT,\
         gmt_offset TEXT,\
         iso_code TEXT,\
         latitude FLOAT,\
         longitude FLOAT,\
         runway_length INT,\
         runway_elevation INT,\
         phone TEXT,\
         fax TEXT,\
         email TEXT,\
         website)"
  curs.execute(q)
  conn.commit()

def fill_table(fname):
  conn,curs = sqlite_db()
  reader = csv.reader(open(fname, 'r', encoding='latin-1'), delimiter=';')

  num = 0
  for line in reader:
    print(line)
    num += 1
    if num == 1:
      # Skip header line
      continue
    iata_code = line[0]
    icao_code = line[1]
    name_eng = line[2]
    city_eng = line[3]
    gmt_offset = line[4]
    iso_code = line[5]
    try:
      latitude = float(line[6])
    except:
      latitude = 0.0
    try:
      longitude = float(line[7])
    except:
      longitude = 0.0
    try:
      runway_length = int(line[8])
    except:
      runway_length = 0
    try:
      runway_elevation = int(line[9])
    except:
      runway_elevation = 0
    phone = line[10]
    fax = line[11]
    email = line[12]
    website = line[13]

    q = "INSERT INTO data\
         (iata_code,icao_code,name_eng,city_eng,\
          gmt_offset,iso_code,\
          latitude,longitude,runway_length,runway_elevation,\
          phone,fax,email,website) VALUES\
         ('"+iata_code+"','"+icao_code+"','"+name_eng+"','"+city_eng+"',\
          '"+gmt_offset+"','"+iso_code+"',\
           "+str(latitude)+","+str(longitude)+","+str(runway_length)+","+str(runway_elevation)+",\
          '"+phone+"','"+fax+"','"+email+"','"+website+"')"
    curs.execute(q)
  conn.commit()

if __name__ == '__main__':

  fname = 'airports.csv'

  create_table()  
  fill_table(fname) 
