from setuptools import setup, find_packages
from distutils.core import setup

setup(
  name='PyAirport',
  version='0.1.5',
  author='Fedor Nikitin',
  author_email='fedor.nikitin@gmail.com',
  url='https://bitbucket.org/sumkin/pyairport/',
  packages=find_packages(),
  package_data={'': ['*.db']},
  license='',
  description='airport codes module',
  long_description=open('README.txt').read(),
)

