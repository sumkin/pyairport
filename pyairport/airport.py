import os
import sys
import csv
import math
import sqlite3
from pkg_resources import resource_filename


class Airport:

    dir_path = os.path.dirname(os.path.realpath(__file__))
    dir_path = dir_path.split("/")[:-1]
    dir_path = "/".join(dir_path)
    fname = dir_path + "/resources/airports.db"
    conn = sqlite3.connect(fname)
    conn.text_factory = str
    c = conn.cursor()


    def __init__(self,code):
        self.code = code.strip()


    def get_city(self):
        self.c.execute("SELECT city_eng FROM data\
                        WHERE iata_code = '" + self.code + "'")
        row = self.c.fetchone()
        return row[0]


    def get_country(self):
        self.c.execute("SELECT country_eng FROM data\
                        WHERE iata_code = '" + self.code + "'")
        row = self.c.fetchone()
        return row[0]


    def get_country_code(self):
        self.c.execute("SELECT iso_code FROM data\
                        WHERE iata_code = '" + self.code + "'")
        row = self.c.fetchone()
        return row[0]


    def get_longitude(self):
        self.c.execute("SELECT longitude FROM data\
                        WHERE iata_code = '" + self.code + "'")
        row = self.c.fetchone()
        return row[0]


    def get_latitude(self):
        self.c.execute("SELECT latitude FROM data\
                        WHERE iata_code = '" + self.code + "'")
        row = self.c.fetchone()
        return row[0]


    def get_gmt_offset(self):
        self.c.execute("SELECT gmt_offset FROM data\
                        WHERE iata_code = '" + self.code + "'")
        row = self.c.fetchone()
        return row[0]


    def distance(self,ap2):
        lat1 = self.get_latitude()
        lat2 = ap2.get_latitude()
        long1 = self.get_longitude()
        long2 = ap2.get_longitude()

        degrees_to_radians = math.pi/180
        phi1 = (90.0 - lat1)*degrees_to_radians
        phi2 = (90.0 - lat2)*degrees_to_radians

        theta1 = long1 * degrees_to_radians
        theta2 = long2 * degrees_to_radians

        cos = (math.sin(phi1) * math.sin(phi2) * math.cos(theta1 - theta2) +\
               math.cos(phi1) * math.cos(phi2))
        arc = math.acos(cos)
        return arc * 6371 # Radius of earth


    """
    @staticmethod
    def get_country_code(apcode):
        Airport.c.execute("SELECT iso_code FROM data\
                           WHERE iata_code = '" + apcode + "'")
        res = Airport.c.fetchone()
        return res[0]
    """


    @staticmethod
    def get_country_rgb(ccode):
        gcc,bcc = ccode[0].lower(), ccode[1].lower()
        gc,bc = ord(gcc) - 96, ord(bcc) - 96
        g,b = int(float(gc) * 255 / 24), int(float(bc) * 255 / 24)
        return 0,g,b


    @staticmethod
    def get_countries():
        Airport.c.execute("SELECT DISTINCT country_eng FROM data")
        ress = Airport.c.fetchall()
        for res in ress:
            if res[0] is not None:
                yield res[0]


    @staticmethod
    def get_cities_in_country(cntry):
        Airport.c.execute("SELECT DISTINCT city_eng\
                           FROM data\
                           WHERE is_code = '" + cntry + "'")
        ress = Airport.c.fetchall()
        for res in ress:
            if res[0] is not None:
              yield res[0]


    @staticmethod
    def get_places(term):
        term = term.lower()
        q = "SELECT iata_code,name_eng,city_eng,country_eng\
             FROM data\
             WHERE lower(iata_code) LIKE '" + term + "%' OR\
                   lower(name_eng) LIKE '" + term + "%' OR\
                   lower(city_eng) LIKE '" + term + "%'\
             ORDER BY runway_length DESC,city_eng,name_eng,country_eng"
        Airport.c.execute(q)
        ress = Airport.c.fetchall()
        for res in ress:
            yield [res[0],res[1],res[2],res[3]]


    @staticmethod
    def sort_nested_rgb(airports):
        apcntr = [[a, Airport.get_country_code(a)] for a in airports]  
        apcntr = [[a, Airport.get_country_rgb(a[1])] for a in apcntr]
        class Comp:
            def __init__(self, tup):
                self.tup = tup

            def __lt__(self, other):
                if self.tup[1] < other.tup[1]:
                    return True
                elif self.tup[1] > other.tup[1]:
                    return False
                else:
                    return self.tup[0] < other.tup[0]
        apcntr = sorted(apcntr, key = Comp)
        return apcntr


    @staticmethod
    def get_airport_country_pairs():
        q = "SELECT iata_code, iso_code FROM data"
        Airport.c.execute(q)
        ress = Airport.c.fetchall()
        return ress


    @staticmethod
    def write_csv_airport_country_pairs(fname):
        with open(fname, 'wb') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['AIRPORT','COUNTRY'])

            pairs = Airport.get_airport_country_pairs()
            for pair in pairs:
                writer.writerow(list(pair))
            


if __name__ == '__main__':
    ap = Airport("HEL")  
    print(ap.get_gmt_offset())  






